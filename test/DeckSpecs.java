import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class DeckSpecs {

    @Test
    public void itGivesTheTopCard() {

        Deck deck = new Deck();
        deck.add(Card.diamond(1));
        deck.add(Card.diamond(2));
        deck.add(Card.diamond(3));
        assertThat(deck.drawTop()).isEqualTo(Card.diamond(1));
        assertThat(deck.drawTop()).isEqualTo(Card.diamond(2));
        assertThat(deck.drawTop()).isEqualTo(Card.diamond(3));

    }
}
