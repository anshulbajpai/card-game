import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class CardSpecs {

    @Test
    public void itComparesCardsOnNumber() {
        assertThat(Card.diamond(1).compareTo(Card.diamond(1))).isEqualTo(0);
        assertThat(Card.diamond(1).compareTo(Card.diamond(2))).isEqualTo(-1);
        assertThat(Card.diamond(2).compareTo(Card.diamond(1))).isEqualTo(1);
    }

    @Test
    public void itComparesCardsOnSuite() {
        assertThat(Card.diamond(1).compareTo(Card.hearts(1))).isEqualTo(1);
        assertThat(Card.hearts(1).compareTo(Card.clubs(1))).isEqualTo(1);
        assertThat(Card.clubs(1).compareTo(Card.spades(1))).isEqualTo(1);
    }
}
