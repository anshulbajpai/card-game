import org.junit.Test;

import java.util.Arrays;

import static org.fest.assertions.Assertions.assertThat;

public class CardGameSpecs {

    @Test
    public void itShufflesCards() {
        Deck deck = getDeck();
        new CardGame(getPlayers(), deck).shuffle();
        assertThat(deck.get(0)).isNotEqualTo(Card.diamond(1));
        assertThat(deck.get(1)).isNotEqualTo(Card.diamond(2));
        assertThat(deck.get(2)).isNotEqualTo(Card.diamond(3));
        assertThat(deck.get(3)).isNotEqualTo(Card.diamond(4));
        assertThat(deck.get(4)).isNotEqualTo(Card.hearts(1));
    }

    @Test
    public void itDistributesOneCardToEachPlayer() {
        Players players = getPlayers();
        CardGame cardGame = new CardGame(players, getDeck());
        cardGame.distribute(1);

        Deck deck = getDeck();

        assertThat(players.get(0).draw()).isEqualTo(deck.drawTop());
        assertThat(players.get(1).draw()).isEqualTo(deck.drawTop());
        assertThat(players.get(2).draw()).isEqualTo(deck.drawTop());
        assertThat(players.get(3).draw()).isEqualTo(deck.drawTop());
    }

    @Test
    public void itDecidesWinnerForOneRoundScenario1() {

        Player expectedWinner = new Player();
        Players players = new Players();
        players.add(new Player());
        players.add(new Player());
        players.add(new Player());
        players.add(expectedWinner);

        Deck deck = new Deck();
        deck.add(Card.clubs(5));
        deck.add(Card.spades(10));
        deck.add(Card.clubs(12));
        deck.add(Card.diamond(2));
        CardGame cardGame = new CardGame(players, deck);

        assertThat(cardGame.play(1)).isEqualTo(Arrays.asList(expectedWinner));
    }

    @Test
    public void itDecidesWinnerForOneRoundScenario2() {

        Player expectedWinner = new Player();
        Players players = new Players();
        players.add(new Player());
        players.add(new Player());
        players.add(expectedWinner);
        players.add(new Player());

        Deck deck = new Deck();
        deck.add(Card.spades(10));
        deck.add(Card.spades(0));
        deck.add(Card.clubs(10));
        deck.add(Card.clubs(9));
        CardGame cardGame = new CardGame(players, deck);

        assertThat(cardGame.play(1)).isEqualTo(Arrays.asList(expectedWinner));
    }

    @Test
    public void itDecidedWinnerForMultipleRoundsScenario1() {
        Player expectedWinner = new Player();
        Players players = new Players();
        players.add(new Player());
        players.add(new Player());
        players.add(expectedWinner);
        players.add(new Player());

        CardGame cardGame = new CardGame(players, getDeckForMultipleRoundScenario1());
        assertThat(cardGame.play(5)).isEqualTo(Arrays.asList(expectedWinner));
    }

    @Test
    public void itDecidedWinnerForMultipleRoundsScenario2() {
        Player expectedWinner1 = new Player();
        Player expectedWinner2 = new Player();
        Players players = new Players();
        players.add(expectedWinner1);
        players.add(new Player());
        players.add(expectedWinner2);
        players.add(new Player());

        CardGame cardGame = new CardGame(players, getDeckForMultipleRoundScenario2());
        assertThat(cardGame.play(5)).contains(expectedWinner1, expectedWinner2);
    }

    private Deck getDeckForMultipleRoundScenario2() {
        Deck deck = new Deck();
        deck.add(Card.clubs(2));
        deck.add(Card.diamond(5));
        deck.add(Card.diamond(12));
        deck.add(Card.spades(6));
        deck.add(Card. hearts(10));
        deck.add(Card.spades(13));
        deck.add(Card.clubs(9));
        deck.add(Card.spades(2));
        deck.add(Card.spades(5));
        deck.add(Card.diamond(3));
        deck.add(Card.hearts(8));
        deck.add(Card.hearts(4));
        deck.add(Card.clubs(11));
        deck.add(Card.spades(12));
        deck.add(Card.diamond(10));
        deck.add(Card.diamond(1));
        deck.add(Card.hearts(12));
        deck.add(Card.clubs(3));
        deck.add(Card.spades(10));
        deck.add(Card.hearts(5));
        return deck;
    }

    private Deck getDeckForMultipleRoundScenario1() {
        Deck deck = new Deck();
        deck.add(Card.diamond(4));
        deck.add(Card.diamond(8));
        deck.add(Card.clubs(6));
        deck.add(Card.clubs(4));
        deck.add(Card. clubs(12));
        deck.add(Card.diamond(12));
        deck.add(Card.diamond(13));
        deck.add(Card.spades(6));
        deck.add(Card.diamond(9));
        deck.add(Card.diamond(6));
        deck.add(Card.spades(8));
        deck.add(Card.hearts(13));
        deck.add(Card.spades(7));
        deck.add(Card.hearts(3));
        deck.add(Card.hearts(4));
        deck.add(Card.clubs(3));
        deck.add(Card.hearts(9));
        deck.add(Card.clubs(5));
        deck.add(Card.diamond(10));
        deck.add(Card.spades(3));
        return deck;
    }

    private Players getPlayers() {
        Players players = new Players();
        players.add(new Player());
        players.add(new Player());
        players.add(new Player());
        players.add(new Player());
        return players;
    }


    private Deck getDeck() {
        Deck deck = new Deck();
        deck.add(Card.diamond(1));
        deck.add(Card.diamond(2));
        deck.add(Card.diamond(3));
        deck.add(Card.diamond(4));
        deck.add(Card.hearts(1));
        return deck;
    }
}
