import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class DrawCardSpecs {

    @Test
    public void itFindsWinnerPlayer() {
        DrawCards drawCards = new DrawCards();
        drawCards.add(new DrawCard(Card.diamond(1), new Player()));
        Player expectedWinner = new Player();
        drawCards.add(new DrawCard(Card.diamond(2), expectedWinner));
        assertThat(drawCards.winner()).isEqualTo(expectedWinner);
    }
}
