import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class PlayersSpecs {

    @Test
    public void itFindsWinner() {
        Players players = new Players();
        Player player1 = new Player();
        player1.accept(Card.diamond(1));
        players.add(player1);
        Player player2 = new Player();
        player2.accept(Card.diamond(2));
        players.add(player2);
        assertThat(players.play()).isEqualTo(player2);
    }
}
