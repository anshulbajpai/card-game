public class CardGame {
    private final Players players;
    private final Deck deck;

    public CardGame(Players players, Deck deck) {
        this.players = players;
        this.deck = deck;
    }

    public void shuffle() {
        deck.shuffle();
    }

    public void distribute(int rounds) {
        for(int i = 0 ; i < rounds; i++){
            for (Player player : players) {
                player.accept(deck.drawTop());
            }
        }
    }

    public Players play(int rounds) {
        distribute(rounds);
        Players roundWinners = new Players();
        for (int i = 0; i < rounds; i++) {
            roundWinners.add(players.play());
        }
        return roundWinners.mostOccurrences();
    }
}
