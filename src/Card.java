public class Card implements Comparable<Card>{
    private final Suite suite;
    private final Integer number;

    private Card(Suite suite, int number) {
        this.suite = suite;
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return number.equals(card.number) && card.suite == suite;
    }

    @Override
    public int hashCode() {
        int result = suite != null ? suite.hashCode() : 0;
        result = 31 * result + number;
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "suite=" + suite +
                ", number=" + number +
                '}';
    }

    @Override
    public int compareTo(Card card) {
        if(suite.equals(card.suite))
            return number.compareTo(card.number);
        return suite.compareTo(card.suite);
    }

    public static Card diamond(int number) {
        return createCard(Suite.DIAMONDS, number);
    }

    public static Card hearts(int number) {
        return createCard(Suite.HEARTS, number) ;
    }

    public static Card clubs(int number) {
        return createCard(Suite.CLUBS, number);
    }

    public static Card spades(int number) {
        return createCard(Suite.SPADES, number);
    }

    private static Card createCard(Suite suite, int number) {
        return new Card(suite, number);
    }
}
