public class Player {

    private Deck deck = new Deck();

    public Card draw() {
        return deck.drawTop();
    }

    public void accept(Card card) {
        deck.add(card);
    }
}
