public class DrawCard implements Comparable<DrawCard>{
    private final Card card;
    private Player player;

    public DrawCard(Card card, Player player) {
        this.card = card;
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public int compareTo(DrawCard drawCard) {
        return card.compareTo(drawCard.card);
    }
}
