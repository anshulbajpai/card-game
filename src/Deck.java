import java.util.ArrayList;
import java.util.Collections;

public class Deck extends ArrayList<Card> {

    public void shuffle() {
        for (int i = 0; i < size(); i++) {
            int j = (int) Math.random() * size();
            Collections.swap(this, i, j);
        }
    }

    public Card drawTop() {
        return remove(0);
    }
}
