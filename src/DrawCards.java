import java.util.ArrayList;
import java.util.Collections;

import static java.util.Collections.sort;

public class DrawCards extends ArrayList<DrawCard>{
    
    public void add(Card card, Player player) {
        add(new DrawCard(card, player));
    }

    public Player winner() {
        sort(this, Collections.reverseOrder());
        return get(0).getPlayer();
    }
}
