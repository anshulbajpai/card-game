import java.util.*;

public class Players extends ArrayList<Player>{

    public Players(Collection<Player> players) {
        super(players);
    }

    public Players() {
    }

    public Player play() {
        DrawCards drawCards = new DrawCards();
        for (Player player : this) {
            drawCards.add(player.draw(), player);
        }
        return drawCards.winner();
    }


    // Refactor this
    public Players mostOccurrences() {
        int maxCount = getMaxCount();
        Set<Player> players = new HashSet<Player>();
        for (Player player : this) {
            if(isWinnerPlayer(player, maxCount))
                players.add(player);
        }
        return new Players(players);
    }

    private boolean isWinnerPlayer(Player player, int maxCount) {
        return Collections.frequency(this, player) == maxCount;
    }

    private int getMaxCount() {
        List<Integer> counts = new ArrayList<Integer>();
        for (Player player : this) {
            counts.add(Collections.frequency(this, player));
        }
        return Collections.max(counts);
    }
}
